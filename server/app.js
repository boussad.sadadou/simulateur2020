const express = require('express');
const app = express();

const port = process.env.PORT || 3000;

const villeRoute = require('./libs/routes/ville.js');
const LibMysql = require("./libs/mysql.js");

const bodyParser = require('body-parser');

//Middleware
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

//Temporaire...
const crossOrigin = require("./libs/middlewares/crossOrigin.js");
app.use(crossOrigin);

const mysqlConnexion = new LibMysql();

function calculData(data)
{
    let municipauxMajoritaire = 0;

    if(data.Nb_elus_municipaux > 4){
        municipauxMajoritaire = Math.ceil(data.Nb_elus_municipaux/2);
    }
    else{
        municipauxMajoritaire = Math.round(data.Nb_elus_municipaux / 2);
    }


    data.siegeMajoritaire = municipauxMajoritaire;
    data.siegeRestant = data.Nb_elus_municipaux - municipauxMajoritaire;
    data.quotientElectoralPremierTour = data.Exprimes_premier_tour / data.siegeRestant;
    data.quotientElectoralDeuxiemeTour = data.Exprimes_deuxieme_tour / data.siegeRestant;

    if(data.Nb_elus_conseilsecteur != 0){
        nb_elus_conseilsecteur = 0;

        if(data.Nb_elus_conseilsecteur > 4){
            nb_elus_conseilsecteur = Math.ceil(data.Nb_elus_conseilsecteur/2);
        }
        else{
            nb_elus_conseilsecteur = Math.floor(data.Nb_elus_conseilsecteur / 2);
        }

        data.siegeSecteurMajoritaire = nb_elus_conseilsecteur;
        data.siegeSecteurRestant = data.Nb_elus_conseilsecteur - nb_elus_conseilsecteur;
        data.quotientElectoralPremierTour = data.Exprimes_premier_tour / data.siegeSecteurRestant;
        data.quotientElectoralDeuxiemeTour = data.Exprimes_deuxieme_tour / data.siegeSecteurRestant;
    }

    return data;
}

app.post('/search/:search',async (req, res) => {

    let search = req.params.search+'%';
    let page = (req.body.page) ? parseInt(req.body.page) : 0;

    search = search.replace(/-/g, ' ');

    try {

        let sql_template = "Select count(Id_ville_insee) AS total from ?? where REPLACE(Nom_ville, '-', ' ') LIKE ? AND `Id_ville_insee` NOT BETWEEN 75101 AND 75120";

        let replaces = ['communes_elus', search];
        let sql = mysqlConnexion.mysql.format(sql_template, replaces);

        let connection = await mysqlConnexion.start();

         const totalData = await mysqlConnexion.queryOne(connection, sql);

         connection.destroy();

         let sql_template_limit = "Select * from ?? where REPLACE(Nom_ville, '-', ' ') LIKE ? AND `Id_ville_insee` NOT BETWEEN 75101 AND 75120 AND `Id_ville_insee` NOT BETWEEN 69381 AND 69389 AND `Id_ville_insee` NOT BETWEEN 13201 AND 13220 ORDER BY Nom_ville LIMIT ?,?";

         replaces = ['communes_elus', search, page * 20, 20];

         sql = mysqlConnexion.mysql.format(sql_template_limit, replaces);

         connection = await mysqlConnexion.start();

         const data = await mysqlConnexion.queryAll(connection, sql);

         connection.destroy();

         let nextPage = -1;
         if(totalData['total'] >= (page +1) * 20)
         {
             nextPage = page + 1;
         }

         res.json({data: data, nextPage: nextPage, pagemax: Math.floor(totalData['total']/20)});
    }
    catch(error) {
        res.json(['une erreur est survenue']);
    }

});
app.get('/:insee', async (req, res) => {

    var insee = req.params.insee;

    try {
        const connection = await mysqlConnexion.start();

        let sql_template = "Select * from ?? where Id_ville_insee = ? ";

        let replaces = ['communes_elus', insee];
        let sql = mysqlConnexion.mysql.format(sql_template, replaces);

         let data = await mysqlConnexion.queryOne(connection, sql);

         connection.destroy();

         data = calculData(data);

         sql_template_secteurs = '';

         if(insee == 75056){
             sql_template_secteurs = "Select * from communes_elus where `Id_ville_insee` BETWEEN 75101 AND 75120 LIMIT 0, 20";
         }
         else if(insee == 69300){
             sql_template_secteurs = "Select * from communes_elus where `Id_ville_insee` BETWEEN 69381 AND 69389 LIMIT 0, 20";
         }
         else if(insee == 13055){
             sql_template_secteurs = "Select * from communes_elus where `Id_ville_insee` BETWEEN 13201 AND 13220 LIMIT 0, 20";
         }


         if(sql_template_secteurs){
            const connection = await mysqlConnexion.start();

             sql = mysqlConnexion.mysql.format(sql_template_secteurs, []);

             data.secteurs = await mysqlConnexion.queryAll(connection, sql);
             data.secteurCalcul = 1;
             data.secteurs.forEach((item, index) => {
                 data.secteurs[index] = calculData(item);
                 data.secteurs[index].secteurCalcul = 1;
             })

             connection.destroy();
         }


         // 10 = pourcentage
         // 368 = total
         //Total : 368 * (10 / 100)

         // SI MA LIGNE EST MAJORITAIRE ALORS municipauxMajoritaire SINON 0


         res.json(data);
    }
    catch(error) {
        let data = {error: 0};
        res.json(data);
    }

});

app.listen(port, function(){
  console.log('listening on *:' + port);
});
