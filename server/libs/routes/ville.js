const routes = require('express').Router();


routes.get('/:insee', async (req, res) => {

    var insee = req.params.insee;

    try {
        const connection = await mysqlConnexion.start();

        let sql_template = "Select * from ?? where Id_ville_insee = ? ";

        let replaces = ['communes_elus', insee];
        let sql = mysqlConnexion.mysql.format(sql_template, replaces);

         const data = await mysqlConnexion.queryOne(connection, sql);

         connection.destroy();

         res.json(data);
    }
    catch(error) {
        //silence
    }


});


module.exports = routes;
