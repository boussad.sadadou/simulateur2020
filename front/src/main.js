import Vue from 'vue'
import App from './App.vue'
import router from "./router";
import vuex from "vuex";
import i18n from './i18n'

import { BootstrapVue, BContainer, BModal, BAlert, BButton, BPopover, BNavbar, BNavbarBrand, BNavbarToggle, BCollapse, BNavItem, BNavbarNav, BNavItemDropdown, BDropdownItem } from 'bootstrap-vue'

Vue.config.productionTip = false

Vue.use(BootstrapVue);

Vue.component('b-alert', BAlert)
Vue.component('b-container', BContainer)
Vue.component('b-popover', BPopover)
Vue.component('b-button', BButton)
Vue.component('b-navbar', BNavbar)
Vue.component('b-navbar-brand', BNavbarBrand)
Vue.component('b-navbar-toggle', BNavbarToggle)
Vue.component('b-collapse', BCollapse)
Vue.component('b-nav-item', BNavItem)
Vue.component('b-navbar-nav', BNavbarNav)
Vue.component('b-nav-item-dropdown', BNavItemDropdown)
Vue.component('b-dropdown-item', BDropdownItem)
Vue.component('b-modal', BModal)

new Vue({
    vuex,
    router,
    i18n,
    render: h => h(App)
}).$mount('#app')
