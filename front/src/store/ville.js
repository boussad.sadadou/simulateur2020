import Vue from "vue";

const villeData = Vue.observable({
    ville_mere: 0,
    ville: 0,
    tour: 0,
    secteur: null,
    currentSuffrages: 0
});

export const villeGetters = {
    ville_mere: () => villeData.ville_mere,
    ville: () => villeData.ville,
    tour: () => villeData.tour,
    secteur: () => villeData.secteur,
    currentSuffrages: () => villeData.currentSuffrages
};

export const villeMethode = {
    setSuffrage(valeur = null) {
        villeData.currentSuffrages = valeur;
    },
    setSecteur(valeur = null) {
        villeData.secteur = valeur;
    },
    setVille(ville, index = null) {

        if(ville == null && index == null){
            villeData.ville = villeData.ville_mere;
        }
        else if(index == null){
            villeData.ville_mere = ville;
            villeData.ville = ville;
        }
        else{
            villeData.ville = villeData.ville_mere.secteurs[index];
        }

    },
    setTour(tour) {
        villeData.tour = tour;
    }
};
