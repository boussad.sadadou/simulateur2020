import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
    routes: [
        {
          path: "/",
          component: () => import("@/views/HomeGlobal"),
          name: "acceuil-recherche"
       },
       {
          path: "/simulateur/:insee",
          component: () => import("@/views/Simulateur"),
          name: "simulateur"
       },
       {
           path: "/methode-calcul",
           name: "methode-calcul",
           component: () => import("@/views/autres/MethodeCalcul")
       },
       {
           path: "/comment-marchent-les-elections",
           name: "comment-marchent-les-elections",
           component: () => import("@/views/autres/CommentMarcheElections")
       },
       {
           path: "/mentions-legales",
           name: "mentionslegales",
           component: () => import("@/views/autres/MentionsLegales")
       },
       {
           path: "/credits",
           name: "credits",
           component: () => import("@/views/autres/Credits")
       },
       {
           path: "/contact",
           name: "contact",
           component: () => import("@/views/autres/Contact")
       }
    ]
});
